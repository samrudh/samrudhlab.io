(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".profile-wrapper {\r\n    width: 100%;\r\n    float: left;\r\n    padding: 1%;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.profile-container {\r\n    width: 50%;\r\n    float: left;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.profile-container:first-child {\r\n    border-right: 2px solid;\r\n}\r\n\r\n.profile-container:nth-child(2) {\r\n    padding-left: 5%;\r\n   \r\n}\r\n\r\n.profile-header {\r\n    width: 100%;\r\n    float: left;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.profile-header h4 {\r\n    margin: 0;\r\n    padding-bottom: 1em;\r\n    text-align: left;\r\n    font-size: 2em;\r\n}\r\n\r\n.methodology-header h4 {\r\n    margin: 0;\r\n    text-align: left;\r\n    font-size: 2em;\r\n   padding-top: 2em;\r\n}\r\n\r\n.profile-content-wrapper {\r\n    width: 100%;\r\n    float: left;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.profile-content-lebel {\r\n    width: 50%;\r\n    float: left;\r\n    box-sizing: border-box;\r\n    text-align: right;\r\n    padding-right: 2em;\r\n    font-weight: bold;\r\n}\r\n\r\n.profile-content-desc {\r\n    width: 50%;\r\n    float: left;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.ex-title h3 {\r\n    font-size: 1em;\r\n    font-weight: bold;\r\n    margin: 0;\r\n}\r\n\r\n.ex-title p {\r\n    font-size: 0.8em;\r\n    font-weight: bold;\r\n    margin: 0;\r\n    padding-top: 1.5em;\r\n}\r\n\r\n.contact-main{\r\n    padding-left:70%;\r\n}\r\n\r\n.competence-main{\r\n    padding-left:60%;\r\n    padding-top: 7%;\r\n    \r\n}\r\n\r\n.experience-main{\r\n    padding-left: 0%;\r\n}\r\n\r\n.structure{\r\n    padding-left: 4%;\r\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-image\">\n  <div class=\"main-text\">\n      <p class=\" name\"><span>Samrudh B N </span></p>\n    <h1 style=\"font-size:30px\"> </h1>\n    <h3>And I'm a Photographer</h3>\n    <img class=\"picture\" height=\"10%\" width=\"20%\" src=\"assets/images/abc.jpg\">\n  </div>\n</div>\n\n\n<div class=\"wrapper-objective\">\n\n  <div class=\"title-objective\">\n    <i class=\"heading_file\"></i>\n    OBJECTIVE\n\n  </div>\n  <h4 class=\"line1\">Secure the position of a digital photographer and utilize my photography skills to capture photographs. To get an opportunity where I can apply my skills to fullest level\n  </h4>\n  <h4 class=\"line2\">and also I can challenge my abilities to the fullest level and gain expertise in this field.</h4>\n\n</div>\n\n\n\n\n\n\n<!-- \n<div class=\"section\">\n  <div id=\"section-left\"> -->\n\n\n<!-- <div class=\"wrapper-left\">\n\n      <div class=\"title-left\">\n        <i class=\"heading_file\"></i>\n        Contact\n      </div>\n    </div>\n    <div id=\"contact\">\n      <table align=\"center\">\n        <tr>\n          <td style=\"text-align: right\">\n            <b>Address:</b>\n          </td>\n          <td class=\"new\">CV Raman Nagar, Bengaluru</td>\n        </tr>\n        <tr>\n          <td style=\"text-align: right\">\n            <b>Tel:</b>\n          </td>\n          <td class=\"newn\">437019</td>\n        </tr>\n        <tr>\n          <td style=\"text-align: right\">\n            <b>Mobile:</b>\n          </td>\n          <td class=\"newn\">9955926257</td>\n        </tr>\n      </table>\n    </div>\n  </div> -->\n<!--right section-->\n<!-- <div id=\"section-right\">\n    <div class=\"wrapper\">\n      <div class=\"section\">\n        <div class=\"title\">\n          <i class=\"heading_file\"></i>\n          Experience\n        </div>\n        <div class=\"content\">\n          <h2 class=\"file\">Backend Development </h2>\n          <h3 class=\"file\" class=\"subdivision\">PwC India | JULY 2018 | Current</h3>\n          <ul>\n            <li>Responsible for designing and implementing API endpoints.\n            </li>\n            <li>Responsible for testing the APIs using Postman, cURL.\n            </li>\n          </ul>\n        </div>\n\n        <div class=\"content\">\n          <h2 class=\"file\">Frontend Development</h2>\n          <h3 class=\"subdivision\">PwC India | MARCH 2018 | Currentt</h3>\n          <ul>\n            <li>Responsible for the designing rules and database tables</li>\n            <li>Responsible for integration of backend with the front-end</li>\n          </ul>\n        </div>\n      </div>\n\n      <div class=\"wrapper-formation-left\">\n\n        <div class=\"title-formation-left\">\n          <i class=\"heading_file\"></i>\n          Education\n        </div>\n      </div>\n      <div id=\"section-sub-left\">\n          <div class=\"content\">\n              <h3 class=\"subdivision\">2018 - Electrical and Electronics Engineering - Birla Institute of Technology</h3>\n              </div>\n\n        \n\n\n\n      </div>\n      \n    </div>\n  </div>\n</div> -->\n\n<div class=\"profile-wrapper\">\n  <div class=\"profile-container\">\n    <div class=\"profile-header contact-main\">\n      <h4>Contact</h4>\n    </div>\n    <div class=\"profile-content-wrapper\">\n      <div class=\"profile-content-lebel\">\n        Address:\n      </div>\n      <div class=\"profile-content-desc\">\n        CV Raman Nagar, Bengaluru\n      </div>\n    </div>\n    <div class=\"profile-content-wrapper\">\n      <div class=\"profile-content-lebel\">\n        Tel:\n      </div>\n      <div class=\"profile-content-desc\">\n        437021\n      </div>\n    </div>\n    <div class=\"profile-content-wrapper\">\n      <div class=\"profile-content-lebel\">\n        Mobile:\n      </div>\n      <div class=\"profile-content-desc\">\n        +91-8277241271\n      </div>\n    </div>\n    <div class=\"profile-content-wrapper\">\n      <div class=\"profile-content-lebel\">\n        Email:\n      </div>\n      <div class=\"profile-content-desc\">\n        samrudh.bn@gmail.com\n      </div>\n    </div>\n    <div class=\"profile-header competence-main\">\n      <h4>Competence</h4>\n    </div>\n    <div class=\"profile-content-wrapper\">\n      <div class=\"profile-img-desc\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/Lightroom.png\" style=\"padding-left: 15%\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/afterlight.png\"style=\"padding-left: 2%\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/snapseed.png\" style=\"padding-left: 2%\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/inksapace.png\" style=\"padding-left: 15%\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/adobe premier Pro.png\"style=\"padding-left: 2%\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/photoshop.png\"style=\"padding-left: 2%\">\n\n\n      </div>\n    </div>\n\n    <div class=\"profile-header competence-main\">\n      <h4>Languages</h4>\n    </div>\n    <div class=\"profile-content-wrapper\">\n      <div class=\"profile-img-desc\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/English.png\" style=\"padding-left: 15%\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/Hindi.png\"style=\"padding-left: 2%\">\n        <img height=\"30%\" width=\"25%\" src=\"assets/images/kannada.png\"style=\"padding-left: 2%\">\n\n\n      </div>\n    </div>\n\n\n\n  </div>\n  <div class=\"profile-container\">\n    <div class=\"profile-header experience-main\">\n      <h4>\n        <i class=\"heading_file\"></i>\n        Experience</h4>\n    </div>\n    <div class=\"profile-content-wrapper structure \">\n      <div class=\"ex-title\">\n        <h3>Assistant Photographer</h3>\n        <p> Discover|JULY 2018 | CURRENT </p>\n      </div>\n      <div class=\"ex-desc\">\n        <ul>\n          <li>Carried out complex photo shoots involving varied types of lighting effects.</li>\n          <li>Experimented with photos using software and brought out interesting effects</li>\n        </ul>\n      </div>\n    </div>\n    <div class=\"profile-content-wrapper structure\">\n      <div class=\"ex-title\">\n        <h3>Assistant Photographer </h3>\n        <p>National Geography | JUNE 2018 | JULY 2018 </p>\n      </div>\n      <div class=\"ex-desc\">\n        <ul>\n          <li>Was part of the outdoor photography team and develop skills in outdoor photography\n          </li>\n          <li>Learnt various photo editing techniques.\n          </li>\n        </ul>\n      </div>\n    </div>\n    <div class=\"profile-content-wrapper structure\">\n\n      <div class=\"ex-title\">\n          <h3>Assistant Photographer</h3>\n          <p>India Today| MAY 2018 | JUNE 2018 </p>\n\n      </div>\n      <div class=\"ex-desc\">\n          <ul>\n              <li>Helped in compiling of studio photo portfolio for nature photography and underwater photography.</li>\n              <li>Learnt various photo editing techniques..</li>\n            </ul>\n      </div>\n    </div>\n    <div class=\"profile-content-wrapper structure\">\n      <div class=\"ex-title\">\n        <h3>Editor </h3>\n        \n      </div>\n      <div class=\"ex-desc\">\n        <ul>\n            <li>Handled model photography and wedding photography assignment\n              </li>\n          \n        </ul>\n      </div>\n    </div>\n\n\n    <div class=\"methodology-header experience-main\">\n      <h4>\n        <i class=\"heading_file\"></i>\n        Education</h4>\n    </div>\n    <div class=\"profile-content-wrapper structure \">\n      <div class=\"ex-title\">\n\n        <p>2018 | Electrical and Electronics | PES University </p>\n      </div>\n\n    </div>\n    <div class=\"methodology-header experience-main\">\n      <h4>\n        <i class=\"heading_file\"></i>\n      Skills</h4>\n    </div>\n    <div class=\"profile-content-wrapper structure \">\n      <div class=\"ex-desc\">\n        <ul>\n          <li>Adobe Lightroom\n            </li>\n          <li>After Light\n            </li>\n        </ul>\n      </div>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\samrudhb932\Documents\assigCV\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map